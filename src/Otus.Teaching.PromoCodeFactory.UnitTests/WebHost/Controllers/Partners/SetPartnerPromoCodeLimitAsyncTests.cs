﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly DateTime ENDDATE = new DateTime(3020, 10, 9);
        private const int LIMIT = 100;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

        }

        private Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 1,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        EndDate = new DateTime(3020, 07, 20),
                        CreateDate = new DateTime(2020, 07, 9),
                        Limit = 100,
                        PartnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d")
                    }
                }
            };

            return partner;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var partnerId = Guid.Empty;
            Partner partner = null;
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = LIMIT
            });
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d");
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = LIMIT
            });

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(10)]
        [InlineData(-10)]
        [InlineData(0)]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNoPositive_ReturnsBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d");
            var partner = CreateBasePartner();
            partner.IsActive = true;

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = limit
            });

            // Assert
            if (limit <= 0)
            {
                result.Should().BeAssignableTo<BadRequestObjectResult>();
            }
        }

        [Theory]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(7)]
        public async void SetPartnerPromoCodeLimitAsync_SetValidLimit_PreviousLimitCanceled(int limit)
        {
            // Arrange
            var limitCodeId = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393");
            var partnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d");
            var partner = CreateBasePartner();
            partner.IsActive = true;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            //Act
            _ = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = limit
            });

            // Assert
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => x.Id == limitCodeId);
            activeLimit.CancelDate.Should().HaveValue();
        }

        [Theory]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(7)]
        public async void SetPartnerPromoCodeLimitAsync_SetValidLimit_SetNumberIssuedPromoCodesToZero(int limit)
        {
            // Arrange
            var limitCodeId = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393");
            var partnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d");
            var partner = CreateBasePartner();
            partner.IsActive = true;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            //Act
            _ = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = limit
            });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(7)]
        public async void SetPartnerPromoCodeLimitAsync_SetValidLimit_NewLimitIsSavedInDB(int limit)
        {
            // Arrange
            var limitCodeId = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393");
            var partnerId = Guid.Parse("aabd2b9f-7a20-4cf6-8a0f-a89432dd3a4d");
            var partner = CreateBasePartner();
            partner.IsActive = true;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            //Act
            _ = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = ENDDATE,
                Limit = limit
            });
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var newLimit = partner.PartnerLimits.FirstOrDefault(x => x.EndDate == ENDDATE && !x.CancelDate.HasValue);
            newLimit.Should().NotBeNull();

        }
    }
}
